try {
  window.angular.module;
} catch(err) {
  console.error('[atx-auth] Looks like you forgot to install angularjs');
}
try {
  window.Firebase.toString();
} catch(err) {
  console.error('[atx-auth] Looks like you forgot to install Firebase');
}
angular.module("templates", []);
angular.module("templates").run(["$templateCache", function($templateCache) {$templateCache.put("atx-login-1.html","<div class=\"fluid-container\">\n    <!-- Latest compiled and minified CSS -->\n    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\"\n          integrity=\"sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7\" crossorigin=\"anonymous\">\n\n    <!-- Optional theme -->\n    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css\"\n          integrity=\"sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r\" crossorigin=\"anonymous\">\n\n    <!-- Latest compiled and minified JavaScript -->\n    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"\n            integrity=\"sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS\"\n            crossorigin=\"anonymous\"></script>\n\n    <link href=\'http://fonts.googleapis.com/css?family=Raleway:500\' rel=\'stylesheet\' type=\'text/css\'>\n\n    <div class=\"panel panel-info\">\n        <div ng-show=\"title\" class=\"panel-heading\">\n            <h3 class=\"panel-title\">{{ title }}</h3>\n        </div>\n        <div class=\"panel-body\">\n            <div class=\"row\">\n\n                <div class=\"col-md-5\">\n                    <a href=\"#\" ng-click=\"fbLogin()\"><img src=\"http://techulus.com/buttons/fb.png\"/></a><br/>\n                    <a href=\"#\" ng-click=\"twLogin()\"><img src=\"http://techulus.com/buttons/tw.png\"/></a><br/>\n                    <a href=\"#\" ng-click=\"gpLogin()\"><img src=\"http://techulus.com/buttons/gplus.png\"/></a><br/>\n                    <a href=\"#\" ng-click=\"emLogin()\"><img src=\"http://techulus.com/buttons/gplus.png\"/></a>\n                </div>\n\n                <div class=\"col-md-7\" style=\"border-left:1px solid #ccc;height:160px\">\n                    <form class=\"form-horizontal\">\n                        <fieldset style=\"display:none\">\n\n                            <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Enter User Name\"\n                                   class=\"form-control input-md\">\n                            <div class=\"spacing\"><input type=\"checkbox\" name=\"checkboxes\" id=\"checkboxes-0\"\n                                                        value=\"1\">\n                                <small> Remember me</small>\n                            </div>\n                            <input id=\"textinput\" name=\"textinput\" type=\"text\" placeholder=\"Enter Password\"\n                                   class=\"form-control input-md\">\n                            <div class=\"spacing\"><a href=\"#\">\n                                <small> Forgot Password?</small>\n                            </a><br/></div>\n                            <button id=\"singlebutton\" name=\"singlebutton\" class=\"btn btn-info btn-sm pull-right\">\n                                Sign In\n                            </button>\n\n\n                        </fieldset>\n                    </form>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n");
$templateCache.put("atx-login-2.html","<style>\n    <!--\n    @import url(https://fonts.googleapis.com/css?family=Roboto:300);\n\n    .login-page {\n        width: 360px;\n        padding: 8% 0 0;\n        margin: auto;\n    }\n    .form {\n        position: relative;\n        z-index: 1;\n        background: #FFFFFF;\n        max-width: 360px;\n        margin: 0 auto 100px;\n        padding: 45px;\n        text-align: center;\n        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);\n    }\n    .form input {\n        font-family: \"Roboto\", sans-serif;\n        outline: 0;\n        background: #f2f2f2;\n        width: 100%;\n        border: 0;\n        margin: 0 0 15px;\n        padding: 15px;\n        box-sizing: border-box;\n        font-size: 14px;\n    }\n    .form button {\n        font-family: \"Roboto\", sans-serif;\n        text-transform: uppercase;\n        outline: 0;\n        background: #4CAF50;\n        width: 100%;\n        border: 0;\n        padding: 15px;\n        color: #FFFFFF;\n        font-size: 14px;\n        -webkit-transition: all 0.3 ease;\n        transition: all 0.3 ease;\n        cursor: pointer;\n    }\n    .form button:hover,.form button:active,.form button:focus {\n        background: #43A047;\n    }\n    .form .message {\n        margin: 15px 0 0;\n        color: #b3b3b3;\n        font-size: 12px;\n    }\n    .form .message a {\n        color: #4CAF50;\n        text-decoration: none;\n    }\n    .form .register-form {\n        display: none;\n    }\n    .container {\n        position: relative;\n        z-index: 1;\n        max-width: 300px;\n        margin: 0 auto;\n    }\n    .container:before, .container:after {\n        content: \"\";\n        display: block;\n        clear: both;\n    }\n    .container .info {\n        margin: 50px auto;\n        text-align: center;\n    }\n    .container .info h1 {\n        margin: 0 0 15px;\n        padding: 0;\n        font-size: 36px;\n        font-weight: 300;\n        color: #1a1a1a;\n    }\n    .container .info span {\n        color: #4d4d4d;\n        font-size: 12px;\n    }\n    .container .info span a {\n        color: #000000;\n        text-decoration: none;\n    }\n    .container .info span .fa {\n        color: #EF3B3A;\n    }\n    body {\n        background: #76b852; /* fallback for old browsers */\n        background: -webkit-linear-gradient(right, #76b852, #8DC26F);\n        background: -moz-linear-gradient(right, #76b852, #8DC26F);\n        background: -o-linear-gradient(right, #76b852, #8DC26F);\n        background: linear-gradient(to left, #76b852, #8DC26F);\n        font-family: \"Roboto\", sans-serif;\n        -webkit-font-smoothing: antialiased;\n        -moz-osx-font-smoothing: grayscale;\n    }\n    -->\n</style>\n<div class=\"login-page\">\n    <div class=\"form\">\n        <form class=\"register-form\">\n            <input type=\"text\" placeholder=\"name\"/>\n            <input type=\"password\" placeholder=\"password\"/>\n            <input type=\"text\" placeholder=\"email address\"/>\n            <button>create</button>\n            <p class=\"message\">Already registered? <a href=\"#\">Sign In</a></p>\n        </form>\n        <form class=\"login-form\">\n            <input type=\"text\" placeholder=\"username\"/>\n            <input type=\"password\" placeholder=\"password\"/>\n            <button>login</button>\n            <p class=\"message\">Not registered? <a href=\"#\">Create an account</a></p>\n        </form>\n    </div>\n</div>");}]);
angular.module('atx-auth', ['templates'])
  .directive('register', function ($q, $compile) {
    return {
      link: function (scope, el, attrs) {

        var layoutOption = attrs.layoutOption;
        var templateUrl = 'atx-'+layoutOption+'.html';

        scope.title = attrs.title;

        el.html("<span ng-include=\"'"+templateUrl+"'\"></span>");

        $compile(el.contents())(scope);

        var url = "https://" + attrs.code + ".firebaseio.com/";
        var ref = new Firebase(url);

        scope.emLogin = loginWith('email', null, null);

        scope.fbLogin = loginWith('facebook');

        scope.gpLogin = loginWith('google');

        scope.twLogin = loginWith('twitter');


        function loginWith(method, providedEmail, providedPassword) {
          return function () {
            if(method === 'email') {

              validateInputs(providedEmail, providedPassword)
                .then(function (validEmail, validPass) {
                  ref.authWithPassword({
                    "email": validEmail,
                    "password": validPass
                  }, handleAuth);
                }, function () {
                  console.warn('Invalid inputs. Email and password are required.');
                });

              function validateInputs(email, pass) {
                return $q(function (resolve, reject) {
                  if(email && pass) {
                    resolve(email, pass);
                  } else {
                    reject()
                  }
                })
              }


            }

            if(method && method !== 'email') {
              ref.authWithOAuthPopup(method, handleAuth);
            }

            function handleAuth (error, authData) {
              if (error) {
                console.log("Login Failed!", error);
              } else {
                console.log("Authenticated successfully with payload:", authData);
              }
            }

          };
        }

      }
    };
  });