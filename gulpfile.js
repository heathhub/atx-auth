var gulp = require('gulp');
var templateCache = require('gulp-angular-templatecache');
var bower = require('gulp-bower');
var concat = require('gulp-concat');

gulp.task('bower', function () {
  bower()
    .pipe(gulp.dest('src/lib'));
});

gulp.task('cache-templates', function () {
  return gulp.src('src/views/**/*.html')
    .pipe(templateCache('compiled_views.js'))
    .pipe(gulp.dest('./src'));
});

gulp.task('copy-demo', function () {
  return gulp.src('./src/index.html')
    .pipe(gulp.dest('./dist'));
});

gulp.task('publish', ['cache-templates', 'copy-demo'], function () {
  return gulp.src(['./src/require_*.js', './src/templates_module.js', './src/compiled_views.js', './src/*.js'])
    .pipe(concat('atx-auth.js'))
    .pipe(gulp.dest('./dist'));
});

gulp.task('default', []);
