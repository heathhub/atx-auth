angular.module('atx-auth', ['templates'])
  .directive('register', function ($q, $compile) {
    return {
      link: function (scope, el, attrs) {

        var layoutOption = attrs.layoutOption;
        var templateUrl = 'atx-'+layoutOption+'.html';

        scope.title = attrs.title;

        el.html("<span ng-include=\"'"+templateUrl+"'\"></span>");

        $compile(el.contents())(scope);

        var url = "https://" + attrs.code + ".firebaseio.com/";
        var ref = new Firebase(url);

        scope.emLogin = loginWith('email', null, null);

        scope.fbLogin = loginWith('facebook');

        scope.gpLogin = loginWith('google');

        scope.twLogin = loginWith('twitter');


        function loginWith(method, providedEmail, providedPassword) {
          return function () {
            if(method === 'email') {

              validateInputs(providedEmail, providedPassword)
                .then(function (validEmail, validPass) {
                  ref.authWithPassword({
                    "email": validEmail,
                    "password": validPass
                  }, handleAuth);
                }, function () {
                  console.warn('Invalid inputs. Email and password are required.');
                });

              function validateInputs(email, pass) {
                return $q(function (resolve, reject) {
                  if(email && pass) {
                    resolve(email, pass);
                  } else {
                    reject()
                  }
                })
              }


            }

            if(method && method !== 'email') {
              ref.authWithOAuthPopup(method, handleAuth);
            }

            function handleAuth (error, authData) {
              if (error) {
                console.log("Login Failed!", error);
              } else {
                console.log("Authenticated successfully with payload:", authData);
              }
            }

          };
        }

      }
    };
  });